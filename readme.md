# Migration Notice
I've moved to a new Git host for my public remote repositories. I will no longer be maintaining code featured on these repositories. You can find the new repo for this project at: https://git.gay/timayo/timayo-web/

The original readme for this repo is below:

---

The source code for Mika's personal website (https://timayo.gay)
Built with my own two paws and hosted on [Nekoweb](https://nekoweb.org)!
